var express = require('express');
var http = require('http')
var socketio = require('socket.io');
var mongojs = require('mongojs');

var ObjectID = mongojs.ObjectID;
var db = mongojs('mongodb://localhost:27017/reactnative');
var app = express();
var server = http.Server(app);
var websocket = socketio(server);
server.listen(3000, () => console.log('listening on *:3000'));

websocket.on('connection', (socket) => {
  socket.on('userJoined', (userId) => onUserJoined(userId, socket));
  socket.on('retrieve_conversation', (data, sendTo) => retrieveConversation(data, socket));
  socket.on('message', (message) => onMessageReceived(message, socket));
});

websocket.on('connect', (socket) => {
  //console.log(socket.id);
});

function onMessageReceived(message, senderSocket) {
  message = message[0];
  var userId = senderSocket.id;
  // Safety check.
  console.log("userId:", userId);
  if (!userId) return;

  _sendAndSaveMessage(message, senderSocket);
}

function _sendAndSaveMessage(message, socket, fromServer) {

  var messageData = {
    text: message.text,
    user: message.user,
    createdAt: new Date(message.createdAt),
  };

  db.collection('messages').insert(messageData, (err, message) => {
    if(err){
      console.log("error", error);
    }else if(message){
      //console.log("data", message);
    }

    db.collection('users')
      .find({"userEmail":message.user.sendTo}, {"_id" : 0, "socketId" : 1})
      .toArray((err, messages) => {
        if (messages[0].socketId == null){
          console.log("sendTo no socketId for " + message.user.sendTo);
        }else{
          console.log("sendTo socketId for " + message.user.sendTo + " is " + messages);
          websocket.to(messages[0].socketId).emit('message', [message]);
        }
      });
  });
}

function onUserJoined(userId, socket) {
  console.log("USER JOINED", userId);
  try {
    if (!userId) {
      console.log("no userId");
      socket.emit('error', "error - no userId/email");
    } else {
      db.collection('users').find({"userEmail":userId}).toArray((err, result) => {
        if (err) {
            console.log('The search errored');
            socket.emit('error_msg', err);
        } else if (result == "") {
            console.log('record not found')
            db.collection('users').insert({"userEmail":userId, "socketId":socket.id}, (err, user) => {
              socket.emit('userJoined', user._id);
              getCurrentUserList(socket, userId);
              //users[socket.id] = user._id;
            });
        } else {
            if(socket.id){
              db.collection('users').update({"userEmail":userId},{$set:{"socketId":socket.id}})
            }
            getCurrentUserList(socket, userId);
        };
      });
    }
  } catch(err) {
    console.log(err);
  }
}

function getCurrentUserList(socket, userId) {
  var messages = db.collection('users')
    .find({ userEmail: { $ne: userId } } )
    .toArray((err, messages) => {
      if (!messages.length) return;
      socket.emit('userList', messages.reverse());
    });
}

function retrieveConversation(data, socket) {
  console.log("retrieveConversation", data.USER + ", " + data.sendToId);
  try {
    if (!data.USER) {
      console.log("Something wrong");
    } else {
      _sendExistingConversation(socket, data);
    }
  } catch(err) {
    console.log(err);
  }
}

function _sendExistingConversation(socket, data) {
  var messages = db.collection('messages')
    .find({ $and: [{"user._id":{ $in: [data.USER,data.sendToId]}},{"user.sendTo":{ $in: [data.USER,data.sendToId]}}]})
    .toArray((err, messages) => {
      if (!messages.length){
        socket.emit('existingConversation', []);
      }else{
        socket.emit('existingConversation', messages.reverse());
      }
    });
}
